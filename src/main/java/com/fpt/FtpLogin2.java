package com.fpt;

import java.io.IOException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;

public class FtpLogin2 {
	public static void main(String[] args) throws IOException {
		FTPClient ftpClient = new FTPClient();
		String errorMessage = null;
		boolean result;
		try {
			// Connect to the localhost
			ftpClient.connect("localhost");
			int reply = ftpClient.getReplyCode();
			
			if (!FTPReply.isPositiveCompletion(reply)) {
				errorMessage = "FTP server refused connection: " + ftpClient.getReplyString();
			} else {
				// login to ftp server
				result = ftpClient.login("admin", "admin");
				if (result == true) {
					System.out.println("Successfully logged in!");
				} else {
					System.out.println("Login Fail!");
					return;
				}
			}
		} catch (FTPConnectionClosedException e) {
			e.printStackTrace();
		} finally {
			try {
				ftpClient.disconnect();
			} catch (FTPConnectionClosedException e) {
				System.out.println(e);
			}
		}
		
	}
}
